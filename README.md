Example of a complex Gutenberg block.
<em>This plugin is a work in progress. Very early. Not complete.</em>

## Development
* `git clone https://gitlab.com/caldera-labs/gutenberg-examples/ex6-events.git`
* `cd ex6-events`
* `npm install`


## Prior Art
This plugin has a lot of copypaste form [https://github.com/youknowriad/gcf](GCF) by [Riad Benguella](https://riad.blog/), which is a very cool plugin to read through as an example of Gutenberg block plugin.