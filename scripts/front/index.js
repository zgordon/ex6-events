import React from "react";
import ReactDOM from "react-dom";
import eventDescriptionRender from "../components/eventDescriptionRender";
import eventLinkRender from "../components/eventLinkRender";
import eventDateRender from "../components/eventDateRender";
import classNames from '../classNames';


document.addEventListener("DOMContentLoaded", function(event) {
    const descriptionBlocks = document.getElementsByClassName(`.${classNames.description}`);
    if ( descriptionBlocks.length) {
        for (let i = 0; i < descriptionBlocks.length; i++) {
            ReactDOM.render(
                eventDescriptionRender({content:descriptionBlocks[i].innerHTML}),
                descriptionBlocks[i]
            );
        }
    }

    const linkBlocks = document.getElementsByClassName(`.${classNames.link}`);
    if ( linkBlocks.length) {
        for (let i = 0; i < linkBlocks.length; i++) {
            ReactDOM.render(
                eventLinkRender({content:linkBlocks[i].innerHTML}),
                linkBlocks[i]
            );
        }
    }

    const dateBlocks = document.getElementsByClassName(`.${classNames.date}`);
    if ( linkBlocks.length) {
        for (let i = 0; i < linkBlocks.length; i++) {
            ReactDOM.render(
                eventDateRender({content:linkBlocks[i].innerHTML}),
                linkBlocks[i]
            );
        }
    }

});


