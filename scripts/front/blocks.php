<?php

function learn_gutenberg_ex6_events_enqueue_block_front_assets() {
    $dir = dirname( __FILE__ );
    $block_js = '/build/index.js';
    $editor_css = '/build/style.css';
    $handle = 'learn-gutenberg/ex6-events-front';

    wp_enqueue_script( $handle, plugins_url( $block_js, __FILE__ ), array(
        'wp-i18n'
    ), md5(filemtime( "$dir/$block_js" ) ));

    wp_enqueue_style( $handle, plugins_url( $editor_css, __FILE__ ), array(
    ), filemtime( "$dir/$editor_css" ) );
}
add_action( 'wp_enqueue_scripts', 'learn_gutenberg_ex6_events_enqueue_block_front_assets' );
