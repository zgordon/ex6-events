//Components for layouts and elements
import {PanelBody, PanelRow, Button, Dashicon} from "@wordpress/components";
//@TODO import from webpack
const __ = wp.i18n.__; //Translation functions from WordPress
import {Component} from "@wordpress/element"; //Extend WordPress component instead of React?
import {Spinner} from "@wordpress/components"; //Spinner component


//Import controls
import {InspectorControls} from "@wordpress/blocks";//Inspector controls has inputs/selects we can reuse
const CheckboxControl = InspectorControls.CheckboxControl;//Native WordPress checkbox control
const SelectControl = InspectorControls.SelectControl;//Native WordPress select control
const TextControl = InspectorControls.TextControl;//Native WordPress select control

//We'll use prop-types after defining class for prop validation/requirements
import PropTypes from 'prop-types';


//Create RSVP Form
class CreateRsvp extends Component {
    constructor(props) {
        super(props);

        //Break props.rsvp up and put in state
        //We can't mutate props.rsvp
        this.state = {
            name: props.rsvp.name,
            email: props.rsvp.name,
            rsvpType: props.rsvp.rsvpType,
        };

        //Bind scope for function
        this.onNameChange = this.onNameChange.bind(this);
        this.onEmailChange = this.onEmailChange.bind(this);
        this.onRsvpTypeChange = this.onRsvpTypeChange.bind(this);
        this.onCreate = this.onCreate.bind(this);

    };

    // Gets the RSVP types or fallback types
    getRsvpTypes() {
        return this.props.rsvpTypes ? this.props.rsvpTypes : [
            {
                value: 'yes',
                label: __('Yes', 'learn-gutenberg')
            },
            {
                value: 'no',
                label: __('No', 'learn-gutenberg')
            },
        ]
    }

    //Update state when name changes
    onNameChange(newValue) {
        this.setState({name: newValue});
    }

    //Update state when email changes
    onEmailChange(newValue) {
        this.setState({email: newValue});
    }

    //Update state when rsvpType changes
    onRsvpTypeChange(newValue) {
        this.setState({rsvpType: newValue});
    }

    //When button is clicked, emit rsvp to parent component
    onCreate(event) {
        const rsvp = {
            learn_gutenberg_ex6_rsvp_name: this.state.name,
            learn_gutenberg_ex6_rsvp_email: this.state.email,
            learn_gutenberg_ex6_rsvp_type: this.state.rsvpType ? this.state.rsvpType : 'yes'
        };

        //Save
        wp.apiRequest({
            method: "POST",
            url: wpApiSettings.root + 'wp/v2/rsvps',
            data: {
                title: this.state.name,
                meta: rsvp,
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
            }
        }).then((body, status, xhr) => {
            //Reset state after save
            this.setState({
                name: '',
                email: '',
                rsvpType: 'no',
            })
        });

    }

    render() {
        return (
            <PanelBody>
                {this.props.isCreating &&
                <PanelRow>
                    <Spinner/>
                </PanelRow>
                }
                {!this.props.isCreating &&
                <PanelRow>
                    <TextControl
                        type={'text'}
                        required={true}
                        label={__('Your Name', 'learn-gutenberg')}
                        value={this.state.name}
                        onChange={this.onNameChange}
                    />

                    <TextControl
                        type={'email'}
                        required={true}
                        label={__('Your Email', 'learn-gutenberg')}
                        value={this.state.rsvp}
                        onChange={this.onEmailChange}
                    />

                    <PanelRow>
                        <SelectControl
                            label={__('RSVP Type')}
                            value={this.state.rsvpType}
                            options={this.getRsvpTypes()}
                            onChange={this.onRsvpTypeChange}
                        />

                        <button
                            onClick={this.onCreate}
                        >
                            <Dashicon
                                icon={'thumbs-up'}
                            />
                            {__('Submit', 'learn-gutenberg')}
                        </button>

                    </PanelRow>


                </PanelRow>
                }


            </PanelBody>
        );
    }


}


//Set property types and make change handlers required
CreateRsvp.propTypes = {
    rsvpTypes: PropTypes.array,
    rsvp: PropTypes.shape({
        rsvpType: PropTypes.string,
        name: PropTypes.string,
        email: PropTypes.string,
        ID: PropTypes.string
    }),

};

//Import higher order component (HOC) to inject WordPress REST API into a component
import {withAPIData} from '@wordpress/components';

//Wrap CreateRsvp with HOC to provide data
export default withAPIData((props) => ( {
    rsvps: `/wp/v2/rsvps`
} ))(CreateRsvp);



