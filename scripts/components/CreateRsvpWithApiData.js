//Import higher order component (HOC) to inject WordPress REST API into a component
import { withAPIData } from '@wordpress/components';

//Import component to wrap
import CreateRsvp from './CreateRsvp';

//Wrap CreateRsvp with HOC to provide data
export default withAPIData( ( props ) => ( {
    rsvps: `/wp/v2/rsvps`
} ) )( CreateRsvp );