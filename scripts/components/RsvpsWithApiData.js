//Import higher order component (HOC) to inject WordPress REST API into a component
import { withAPIData } from '@wordpress/components';

//Import component to wrap
import { Rsvps } from './Rsvps';

//Wrap Rsvps with HOC to provide data
export default withAPIData( ( props ) => ( {
    //Set the API route to use
    rsvps: `/wp/v2/rsvps`
} ) )( Rsvps );