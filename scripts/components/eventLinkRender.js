import classNames from '../classNames';

/**
 * Create preview/display view for URLs
 *
 * @param {Object} attributes
 * @returns {XML}
 */
const eventUrlRender = (attributes) => {
    return (
        <a
            className={classNames.link}
        >
            {attributes.content}
        </a>
    );
};

export default  eventUrlRender;