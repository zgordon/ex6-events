const __ = wp.i18n.__; //Translation functions from WordPress
import { Component } from "@wordpress/element"; //Extend WordPress component instead of React?
import {Spinner, Dashicon} from "@wordpress/components";
import React from 'react'
const el = React.createElement;
import classNames from '../classNames';

//Loop through rsvps
export const Rsvps = (props) => {

    //Utility function to get name from meta, then title, then slug
    function getName(rsvp){
        return (rsvp.meta && rsvp.meta.learn_gutenberg_ex6_rsvp_name )
            ? rsvp.meta.learn_gutenberg_ex6_rsvp_name
            : rsvp.title.rendered
            ? rsvp.title.rendered
            : rsvp.name;
    }

    //Utility function to get type from meta safely
    function getType(rsvp) {
        return (rsvp.meta && rsvp.meta.learn_gutenberg_ex6_rsvp_type)
        ? rsvp.meta.learn_gutenberg_ex6_rsvp_type
            : 'no';
    }

    //Show spinner while loading
    if( props.rsvps.isLoading ){
        return el(
            'Spinner'
        )
    }
    //We have Rsvps?
    else if( props.rsvps.data ) {
        //Collect array of RSVPs
        let els = [];
        props.rsvps.data.forEach((rsvp) => {
            //Push element into array
            els.push( el( 'div', {
                key:rsvp.ID,
                className: classNames.rsvp,
            }, [
                el(
                    'p',
                    {},
                    [
                        el(
                            'span',
                            {
                                className: classNames.rsvpName
                            },
                            getName(rsvp)
                        ),
                        el(
                            Dashicon,
                            {
                                className: classNames.rsvpType,
                                icon: 'yes' === getType(rsvp) ? 'thumbs-up' : 'thumbs-down'
                            },

                        ),
                    ]
                )
            ] ));
        });
        //Returned wrapped in one element
        return el('div', {
            className: classNames.rsvps,
        }, els );
    }else{
        return null;
    }

};
