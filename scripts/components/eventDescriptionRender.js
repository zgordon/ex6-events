import classNames from '../classNames';

/**
 * Create preview/display view for event description
 *
 * @param {Object} attributes
 * @returns {XML}
 */
const eventDescriptionRender = (attributes) => {
    return (
        <div
            className={classNames.description}
        >
            {attributes.content}
        </div>
    );
};

export default  eventDescriptionRender;