import classNames from '../classNames';
import Moment from 'moment'
import { settings } from "@wordpress/date";
//@todo webpack
const __ = wp.i18n.__;

/**
 * Create preview/display view for datetimes of events
 *
 * @param {Object} attributes
 * @returns {XML}
 */
const eventDateRender = (attributes) => {

    if ( attributes.content) {
        let date = Moment(attributes.content)
            .locale( settings.l10n.locale );
        return (
            <div
                date={attributes.content}
                className={classNames.date}
            >
                {date.toString()}
            </div>
        );
    }else  if( attributes.showPlaceHolder ){
        return (
            <div
                date={''}
                className={classNames.date}
            >
                {__( 'Select An Event Date', 'learn-gutenberg' ) }
            </div>
        );
    }else {
        return (
            <div
                date={attributes.content}
                className={classNames.date}
            >
                {attributes.content}
            </div>
        );
    }

};

export default  eventDateRender;