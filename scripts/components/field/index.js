import React from "react";
import { withInstanceId } from "@wordpress/components";

import "./style.scss";
function Field({ instanceId, label, children,labelBefore = true }) {
  const id = `learn-gutenberg-events-${instanceId}`;
  return (
    <div className="learn-gutenberg-events-field">
        {labelBefore &&
            <label htmlFor={id}>{label}</label>
        }
        {children(id)}
        {! labelBefore &&
            <label htmlFor={id}>{label}</label>
        }
    </div>
  );
}

export default withInstanceId(Field);
