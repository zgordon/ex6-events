//@TODO import from webpack
const __ = wp.i18n.__; //Translation functions from WordPres
import React from "react";
import eventDateRender from '../components/eventDateRender' //Render function for save callback and front-end.
import classNames from '../classNames'; //Class names

import { kebabCase } from "lodash";

import { dateTimeField } from "../fields/datetime";
export const dateBlockName = 'learn-gutenberg/ex6-events-date';
export const dateBlock = dateTimeField.getBlockSettings({
    id: dateBlockName,
    content: __( 'Date for Event', 'learn-gutenberg'),
    title: __( 'Event Date', 'learn-gutenberg'),
    slug: kebabCase(dateBlockName),
    render: eventDateRender,
    useOnce: true,
    contentAttribute: {
        type: 'text',
        selector: `.${classNames.date}`,
    }
},dateBlockName );


