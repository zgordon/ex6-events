//@TODO import from webpack
const __ = wp.i18n.__; //Translation functions from WordPress
import React from "react";
import eventDateRender from '../components/eventDateRender' //Render function for save callback and front-end.
import classNames from '../classNames'; //Class names

import {textField} from "../fields/text";

export const rsvpBlockName = 'learn-gutenberg/ex6-events-rsvp';
export let rsvpBlock = textField.getBlockSettings({
    id: rsvpBlockName,
    title: __('RSVP BLOCK', 'learn-gutenberg'),
    slug: 'learn-gutenberg-ex6-events',
    render: function () {
        return '<div>RSVPREMNDER</div>'
    },
    useOnce: true,
    contentAttribute: {
        type: 'text',
        selector: `.${classNames.description}`,
    }
}, rsvpBlockName);


import CreateRsvp from '../components/CreateRsvpWithApiData';
import Rsvps from '../components/RsvpsWithApiData';
rsvpBlock.edit = ({attributes, setAttributes}) => {

    const cb = (event) =>{
        console.log(event);
    };
    return (
        <div>
            <Rsvps/>
            <CreateRsvp
                rsvp={{
                    name: 'name'
                }}
            />

        </div>
    )
};