//Import block registration API form WordPress
import { registerBlockType } from "@wordpress/blocks";

//Register Decision Block
import {descriptionBlockName,descriptionBlock} from "./description";
registerBlockType(descriptionBlockName,descriptionBlock);
//Register Link Block
import {linkBlockName,linkBlock} from "./link";
registerBlockType(linkBlockName,linkBlock);
//Register Date Block
import {dateBlockName,dateBlock} from "./date";
registerBlockType(dateBlockName,dateBlock);
//Register RSVP block
import {rsvpBlockName,rsvpBlock} from "./rsvp";
registerBlockType(rsvpBlockName,rsvpBlock);




