//@TODO import from webpack
const __ = wp.i18n.__;
import React from "react";
import eventDescriptionRender from '../components/eventDescriptionRender'
import classNames from '../classNames';

import { textField } from "../fields/text";
export const descriptionBlockName = 'learn-gutenberg/ex6-events-description';
export const descriptionBlock = textField.getBlockSettings({
    id: descriptionBlockName,
    content: __( 'Description of Event', 'learn-gutenberg'),
    title: __( 'Event Description', 'learn-gutenberg'),
    slug: 'learn-gutenberg-ex6-events',
    render: eventDescriptionRender,
    useOnce: true,
    contentAttribute: {
        type: 'text',
        selector: `.${classNames.description}`,
    }
},descriptionBlockName );

