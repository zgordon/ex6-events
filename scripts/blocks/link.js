//@TODO import from webpack
const __ = wp.i18n.__;
import React from "react";
import eventLinkRender from '../components/eventLinkRender'
import classNames from '../classNames';


import { urlField } from "../fields/url";
export const linkBlockName = 'learn-gutenberg/ex6-events-link';
export const linkBlock = urlField.getBlockSettings({
    id: linkBlockName,
    content: __( 'Page for Event', 'learn-gutenberg'),
    title: __( 'Event Link', 'learn-gutenberg'),
    slug: 'learn-gutenberg-ex6-events-link',
    render: eventLinkRender,
    useOnce: true,
    contentAttribute: {
        type: 'text',
        selector: `.${classNames.link}`,
    }
},linkBlockName );


