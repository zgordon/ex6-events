import React from "react";

import Field from "../components/field";
import basicFieldArgs from  './util'


export const textField = {
    getBlockSettings(fieldConfig, blockName) {
        return Object.assign(basicFieldArgs(fieldConfig,blockName),
            {
                edit({attributes, setAttributes, className, focus, id}) {
                    const blockId = `${fieldConfig.slug}-${id}`;
                    return (
                        <div>
                            {focus &&
                            <Field label={fieldConfig.title || fieldConfig.name}
                                   instanceId={blockId}
                                   labelBefore={false}>
                                {id => (
                                    <input
                                        id={id}
                                        type="text"
                                        value={attributes.content || ""}
                                        onChange={event => {
                                            setAttributes({content: event.target.value});
                                        }}
                                        placeholder="Write"
                                    />
                                )}
                            </Field>
                            }
                            {!focus &&
                                fieldConfig.render(attributes, className)
                            }
                        </div>
                    );
                },
                save({attributes, className}) {
                    return fieldConfig.render(attributes, className);
                },

            }
        );
    }
};

