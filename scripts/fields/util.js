const basicFieldArgs = (fieldConfig,blockName) => {
    return {
        category: fieldConfig.category || 'common',
        icon: fieldConfig.icon || "text",
        title: fieldConfig.title,
        isPrivate: fieldConfig.isPrivate || false,
        supports: fieldConfig.supports || {
            html: true
        },
        attributes: {
            content: fieldConfig.contentAttribute
        },
        useOnce: fieldConfig.useOnce || false,
    };
};

export default basicFieldArgs;