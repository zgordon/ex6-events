import React from "react";

import { BlockControls } from "@wordpress/blocks";
import { DateTimePicker } from "@wordpress/components";
import { settings } from "@wordpress/date";
import { InspectorControls } from "@wordpress/blocks";
import { PanelBody, PanelRow } from "@wordpress/components";


import Field from "../components/field";
import basicFieldArgs from  './util'

//@see https://github.com/youknowriad/gcf/blob/master/scripts/blocks/fields/datetime.js#L9
const is12HourTime = /a(?!\\)/i.test(
    settings.formats.time
        .toLowerCase()
        .replace(/\\\\/g, "")
        .split("")
        .reverse()
        .join("")
);

export const dateTimeField = {
    getBlockSettings(fieldConfig, blockName) {
        return Object.assign(basicFieldArgs(fieldConfig,blockName),
            {
                edit({attributes, setAttributes, className, focus, id}) {
                    const blockId = `${fieldConfig.slug}-${id}`;
                    return (
                        <div>
                            {focus &&
                                <InspectorControls
                                    key={'inspector'}
                                >
                                    <PanelBody>
                                        <PanelRow>
                                            <Field label={fieldConfig.title || fieldConfig.name}>
                                                {() => (
                                                    <DateTimePicker
                                                        locale={settings.l10n.locale}
                                                        currentDate={attributes.datetime}
                                                        is12HourTime={is12HourTime}
                                                        onChange={datetime => {
                                                            setAttributes({content:datetime});
                                                        }}
                                                    />
                                                )}
                                            </Field>
                                        </PanelRow>
                                    </PanelBody>
                                </InspectorControls>
                            }
                            {attributes.content &&
                                fieldConfig.render(Object.assign({showPlaceholder:true},attributes), className)
                            }

                        </div>
                    );
                },
                save({attributes, className}) {
                    return fieldConfig.render(attributes, className);
                },

            }
        );
    }
};
