import React from "react";

import Field from "../components/field";
import basicFieldArgs from  './util'

const {__} = wp.i18n; // WordPress translation functions

export const urlField = {
    getBlockSettings(fieldConfig, blockName) {
        return Object.assign(basicFieldArgs(fieldConfig,blockName),
            {
                edit({attributes, setAttributes, className, focus, id}) {
                    const blockId = `${fieldConfig.slug}-${id}`;
                    return (
                        <div>
                            {focus &&
                                <Field
                                    label={fieldConfig.title || fieldConfig.name}
                                    instanceId={blockId}
                                    labelBefore={false}>
                                    {id => (
                                        <input
                                            id={id}
                                            type="url"
                                            value={attributes.content || ""}
                                            onChange={event => {
                                                setAttributes({content: event.target.value});
                                            }}
                                            placeholder={__( 'https://eventurl.com', 'learn-gutenberg' ) }
                                        />
                                    )}
                                </Field>
                            }
                            {!focus &&
                                fieldConfig.render(attributes, className)
                            }
                        </div>
                    );
                },
                save({attributes, className}) {
                    return fieldConfig.render(attributes, className);
                },

            }
        );
    }
};

